import { Serber } from './serber';

export type TypeofSerberPluginAdapter = new (serber: Serber) => ISerberPlugin;

interface SerberStandartPluginOption {
  SERBER_KEY?: string;
  SERBER_INDEX?: number;
  SERBER_PATH?: string;
}

export type SerberPluginOption = SerberStandartPluginOption & { [key: string]: any };

export interface ISerberPlugin {
  isInitial(obj: any, options?: SerberPluginOption): boolean;
  isOutput(obj: any, options?: SerberPluginOption): boolean;
  serialize(obj: any, options?: SerberPluginOption): any;
  deserialize(obj: any, options?: SerberPluginOption): any;
  isInitialAsync?(obj: any, options?: SerberPluginOption): boolean | Promise<boolean>;
  isOutputAsync?(obj: any, options?: SerberPluginOption): boolean | Promise<boolean>;
  serializeAsync?(obj: any, options?: SerberPluginOption): any;
  deserializeAsync?(obj: any, options?: SerberPluginOption): any;
}

export class SerberPluginAdapter implements ISerberPlugin {
  protected serber: Serber = null;

  constructor(serber: Serber) {
    this.serber = serber;
  }

  isInitial(obj: any, options?: SerberPluginOption) {
    return false;
  }
  isOutput(obj: any, options?: SerberPluginOption) {
    return false;
  }
  serialize(obj: any, options?: SerberPluginOption) {
    return obj;
  }
  deserialize(obj: any, options?: SerberPluginOption) {
    return obj;
  }
}
