import { SerberPluginAdapter } from "../abstract";

export default class NullAdapter extends SerberPluginAdapter {
  isInitial(obj): obj is null {
    return obj === null;
  }

  isOutput(obj): obj is null {
    return obj === null;
  }
}
