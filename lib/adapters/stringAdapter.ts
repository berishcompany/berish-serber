import { SerberPluginAdapter } from "../abstract";

export default class StringAdapter extends SerberPluginAdapter {
  isInitial(obj): obj is string {
    return typeof obj == 'string';
  }

  isOutput(obj): obj is string {
    return typeof obj == 'string';
  }
}
