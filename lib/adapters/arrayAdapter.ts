import { SerberPluginAdapter, SerberPluginOption } from '../abstract';

export default class ArrayAdapter<T> extends SerberPluginAdapter {
  isInitial(obj: Array<T>): obj is Array<T> {
    return Array.isArray(obj);
  }

  isOutput(obj: Array<T>): obj is Array<T> {
    return Array.isArray(obj);
  }

  serialize(obj: Array<T>, option?: SerberPluginOption) {
    return obj.map((m, SERBER_INDEX) => {
      let SERBER_PATH = (option && option.SERBER_PATH) || '';
      if (SERBER_PATH) {
        SERBER_PATH = `${SERBER_PATH}.${SERBER_INDEX}`;
      } else SERBER_PATH = `${SERBER_INDEX}`;
      let nextOption: SerberPluginOption = {
        SERBER_PATH,
        SERBER_KEY: null,
        SERBER_INDEX
      };
      return this.serber.serialize(m, Object.assign({}, option, nextOption));
    });
  }

  serializeAsync(obj: Array<T>, option?: SerberPluginOption) {
    return Promise.all(
      obj.map((m, SERBER_INDEX) => {
        let SERBER_PATH = (option && option.SERBER_PATH) || '';
        if (SERBER_PATH) {
          SERBER_PATH = `${SERBER_PATH}.${SERBER_INDEX}`;
        } else SERBER_PATH = `${SERBER_INDEX}`;
        let nextOption: SerberPluginOption = {
          SERBER_PATH,
          SERBER_KEY: null,
          SERBER_INDEX
        };
        return this.serber.serializeAsync(m, Object.assign({}, option, nextOption));
      })
    );
  }

  deserialize(obj: Array<T>, option?: SerberPluginOption) {
    return obj.map((m, SERBER_INDEX) => {
      let SERBER_PATH = (option && option.SERBER_PATH) || '';
      if (SERBER_PATH) {
        SERBER_PATH = `${SERBER_PATH}.${SERBER_INDEX}`;
      } else SERBER_PATH = `${SERBER_INDEX}`;
      let nextOption: SerberPluginOption = {
        SERBER_PATH,
        SERBER_KEY: null,
        SERBER_INDEX
      };
      return this.serber.deserialize(m, Object.assign({}, option, nextOption));
    });
  }

  deserializeAsync(obj: Array<T>, option?: SerberPluginOption) {
    return Promise.all(
      obj.map((m, SERBER_INDEX) => {
        let SERBER_PATH = (option && option.SERBER_PATH) || '';
        if (SERBER_PATH) {
          SERBER_PATH = `${SERBER_PATH}.${SERBER_INDEX}`;
        } else SERBER_PATH = `${SERBER_INDEX}`;
        let nextOption: SerberPluginOption = {
          SERBER_PATH,
          SERBER_KEY: null,
          SERBER_INDEX
        };
        return this.serber.deserializeAsync(m, Object.assign({}, option, nextOption));
      })
    );
  }
}
