import { SerberPluginAdapter } from "../abstract";

export default class BoolAdapter extends SerberPluginAdapter {
  isInitial(obj): obj is boolean {
    return obj === true || obj === false;
  }

  isOutput(obj): obj is boolean {
    return obj === true || obj === false;
  }
}
