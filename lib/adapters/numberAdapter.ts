import { SerberPluginAdapter } from "../abstract";

export default class NumberAdapter extends SerberPluginAdapter {
  isInitial(obj): obj is number {
    return typeof obj == 'number';
  }

  isOutput(obj): obj is number {
    return typeof obj == 'number';
  }
}
