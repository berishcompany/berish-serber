import { SerberPluginAdapter, SerberPluginOption } from '../abstract';

export default class ObjectAdapter extends SerberPluginAdapter {
  isInitial(obj: Object): obj is Object {
    return obj === Object(obj) && Object.prototype.toString.call(obj) !== '[object Array]';
  }

  isOutput(obj: Object): obj is Object {
    return obj === Object(obj) && Object.prototype.toString.call(obj) !== '[object Array]';
  }

  serialize(obj: Object, option?: SerberPluginOption) {
    let temp: { [params: string]: any } = {};
    for (let SERBER_KEY in obj) {
      let SERBER_PATH = (option && option.SERBER_PATH) || '';
      if (SERBER_PATH) {
        SERBER_PATH = `${SERBER_PATH}.${SERBER_KEY}`;
      } else SERBER_PATH = `${SERBER_KEY}`;
      let nextOption: SerberPluginOption = {
        SERBER_PATH,
        SERBER_KEY,
        SERBER_INDEX: null
      };
      temp[SERBER_KEY] = this.serber.serialize(obj[SERBER_KEY], Object.assign({}, option, nextOption));
    }
    return temp;
  }

  async serializeAsync(obj: Object, option?: SerberPluginOption) {
    let temp: { [params: string]: any } = {};
    for (let SERBER_KEY in obj) {
      let SERBER_PATH = (option && option.SERBER_PATH) || '';
      if (SERBER_PATH) {
        SERBER_PATH = `${SERBER_PATH}.${SERBER_KEY}`;
      } else SERBER_PATH = `${SERBER_KEY}`;
      let nextOption: SerberPluginOption = {
        SERBER_PATH,
        SERBER_KEY,
        SERBER_INDEX: null
      };
      temp[SERBER_KEY] = await this.serber.serializeAsync(obj[SERBER_KEY], Object.assign({}, option, nextOption));
    }
    return temp;
  }

  deserialize(obj: Object, option?: SerberPluginOption) {
    let temp: { [params: string]: any } = {};
    for (let SERBER_KEY in obj) {
      let SERBER_PATH = (option && option.SERBER_PATH) || '';
      if (SERBER_PATH) {
        SERBER_PATH = `${SERBER_PATH}.${SERBER_KEY}`;
      } else SERBER_PATH = `${SERBER_KEY}`;
      let nextOption: SerberPluginOption = {
        SERBER_PATH,
        SERBER_KEY,
        SERBER_INDEX: null
      };
      temp[SERBER_KEY] = this.serber.deserialize(obj[SERBER_KEY], Object.assign({}, option, nextOption));
    }
    return temp;
  }

  async deserializeAsync(obj: Object, option?: SerberPluginOption) {
    let temp: { [params: string]: any } = {};
    for (let SERBER_KEY in obj) {
      let SERBER_PATH = (option && option.SERBER_PATH) || '';
      if (SERBER_PATH) {
        SERBER_PATH = `${SERBER_PATH}.${SERBER_KEY}`;
      } else SERBER_PATH = `${SERBER_KEY}`;
      let nextOption: SerberPluginOption = {
        SERBER_PATH,
        SERBER_KEY,
        SERBER_INDEX: null
      };
      temp[SERBER_KEY] = await this.serber.deserializeAsync(obj[SERBER_KEY], Object.assign({}, option, nextOption));
    }
    return temp;
  }
}
