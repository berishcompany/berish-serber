import { SerberPluginAdapter } from "../abstract";

export default class UndefinedAdapter extends SerberPluginAdapter {
  isInitial(obj): obj is undefined {
    return typeof obj == 'undefined';
  }

  isOutput(obj): obj is undefined {
    return typeof obj == 'undefined';
  }
}
