import { SerberPluginAdapter } from '../abstract';

const DATE_TYPE_OUTPUT = '__Date';

export interface IOutputDate {
  type: string;
  value: number;
}

export default class DateAdapter extends SerberPluginAdapter {
  isInitial(obj: Date): obj is Date {
    return Object.prototype.toString.call(obj) === '[object Date]';
  }

  isOutput(obj: IOutputDate): obj is IOutputDate {
    return (
      obj != null &&
      'type' in obj &&
      'value' in obj &&
      obj.type == DATE_TYPE_OUTPUT &&
      !!obj.value
    );
  }

  serialize(obj: Date) {
    let value = obj.valueOf();
    return {
      type: DATE_TYPE_OUTPUT,
      value
    };
  }

  deserialize(obj: IOutputDate) {
    return new Date(obj.value);
  }
}
