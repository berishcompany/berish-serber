import { TypeofSerberPluginAdapter, SerberPluginOption } from './abstract';
import * as ringle from 'berish-ringle';
import * as Adapters from './adapters';

type ErrorCallback = (err: Error) => any;

export class Serber {
  private _plugins: TypeofSerberPluginAdapter[] = [];
  private _error: ErrorCallback = null;

  private callError(error: Error) {
    if (this._error) this._error(error);
    else console.error(error);
  }

  scope(scope: string) {
    return ringle.getSingleton(Serber, scope);
  }

  plugin(value: TypeofSerberPluginAdapter | TypeofSerberPluginAdapter[]) {
    let values: TypeofSerberPluginAdapter[] = [];
    if (Array.isArray(value)) values = value;
    else values = [value];
    for (let val of values) {
      if (this._plugins.indexOf(val) == -1) this._plugins.push(val);
    }
    return this;
  }

  error(cb: ErrorCallback) {
    this._error = cb;
    return this;
  }

  serialize(obj: any, options?: SerberPluginOption) {
    try {
      for (let plugin of this._plugins) {
        try {
          let instance = ringle.getSingleton(plugin, null, this);
          if (instance.isInitial(obj, options)) {
            let result = instance.serialize(obj, options);
            return result;
          }
        } catch (err) {
          this.callError(err);
        }
      }
    } catch (err) {
      this.callError(err);
    }
    return obj;
  }

  async serializeAsync(obj: any, options?: SerberPluginOption) {
    try {
      for (let plugin of this._plugins) {
        try {
          let instance = ringle.getSingleton(plugin, null, this);
          let isInitial = !!instance.isInitialAsync
            ? await instance.isInitialAsync(obj, options)
            : instance.isInitial(obj, options);
          if (isInitial) {
            let result = !!instance.serializeAsync
              ? await instance.serializeAsync(obj, options)
              : instance.serialize(obj, options);
            return result;
          }
        } catch (err) {
          this.callError(err);
        }
      }
    } catch (err) {
      this.callError(err);
    }
    return obj;
  }

  deserialize(obj: any, options?: SerberPluginOption) {
    try {
      for (let plugin of this._plugins) {
        try {
          let instance = ringle.getSingleton(plugin, null, this);
          if (instance.isOutput(obj, options)) {
            let result = instance.deserialize(obj, options);
            return result;
          }
        } catch (err) {
          this.callError(err);
        }
      }
    } catch (err) {
      this.callError(err);
    }
    return obj;
  }

  async deserializeAsync(obj: any, options?: SerberPluginOption) {
    try {
      for (let plugin of this._plugins) {
        try {
          let instance = ringle.getSingleton(plugin, null, this);
          let isOutput = !!instance.isOutputAsync
            ? await instance.isOutputAsync(obj, options)
            : instance.isOutput(obj, options);
          if (isOutput) {
            let result = !!instance.deserializeAsync
              ? await instance.deserializeAsync(obj, options)
              : instance.deserialize(obj, options);
            return result;
          }
        } catch (err) {
          this.callError(err);
        }
      }
    } catch (err) {
      this.callError(err);
    }
    return obj;
  }

  get plugins() {
    return Adapters;
  }

  get defaults() {
    return [
      this.plugins.UndefinedAdapter,
      this.plugins.NullAdapter,
      this.plugins.BoolAdapter,
      this.plugins.NumberAdapter,
      this.plugins.StringAdapter,
      this.plugins.DateAdapter,
      this.plugins.ArrayAdapter
    ];
  }
}
